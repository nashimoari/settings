<?php

namespace Nashimoari;


use Illuminate\Support\Facades\DB;
use Throwable;

class Settings
{
    private $tableName = 'settings';

    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    public function get($code, $paramName)
    {
        $sql = "select val from {$this->tableName} where code = :code and paramName = :paramName";
        $data = DB::selectOne($sql, ['code' => $code, 'paramName' => $paramName]);

        if (is_null($data)) {
            throw new \Exception('Parameter with the code is not exist');
        }

        return $data->val;
    }

    public function add($code, $paramName, $val)
    {
        $sql = "insert into {$this->tableName} (code, paramName, val) values (:code, :paramName, :val)";
        try {
            DB::insert($sql, ['code' => $code, 'paramName' => $paramName, 'val' => $val]);
        } catch (Throwable $e) {
            return false;
        }
        return true;
    }

    public function delete($code, $paramName)
    {
        $sql = "delete from {$this->tableName} where code = :code and $paramName = :paramName";
        try {
            DB::delete($sql, ['code' => $code, 'paramName' => $paramName]);
        } catch (Throwable $e) {
            return false;
        }
        return true;
    }

    public function update($code, $paramName, $val)
    {
        try {
            $this->get($code, $paramName);
            $sql = "update {$this->tableName} set val = :val where code = :code and paramName = :paramName";
            DB::update($sql, ['code' => $code, 'paramName' => $paramName, 'val' => $val]);
            return true;
        } catch (\Exception $e) {
            return $this->add($code, $paramName, $val);
        }

    }

}
